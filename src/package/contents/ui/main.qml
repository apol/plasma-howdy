/*
 * Copyright (C) 2020 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
*/

import QtQuick 2.1
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.3 as QQC2
import org.kde.kirigami 2.6 as Kirigami
import org.kde.kcm 1.2

SimpleKCM {
    ConfigModule.buttons: ConfigModule.Default | ConfigModule.Apply
    leftPadding: width * 0.1
    rightPadding: leftPadding

    implicitWidth: Kirigami.Units.gridUnit * 38
    implicitHeight: Kirigami.Units.gridUnit * 35


    ColumnLayout {
        Kirigami.InlineMessage {
            id: infoLabel
            Layout.fillWidth: true

            type: Kirigami.MessageType.Information
            visible: !kcm.isHowdyAvailable
            text: i18n("Howdy needs installing")
        }

        QQC2.CheckBox {
            text: i18n("Enabled")
            checked: kcm.isHowdyEnabled
            onCheckedChanged: {
                kcm.isHowdyEnabled = !kcm.isHowdyEnabled
            }
        }

        QQC2.Button {
            Layout.alignment: Qt.AlignHCenter
            text: i18n("Add model")
            onClicked: kcm.addModel(name.text)
        }
        QQC2.TextField {
            Layout.alignment: Qt.AlignHCenter
            id: name
        }

        QQC2.Button {
            Layout.alignment: Qt.AlignHCenter
            text: i18n("Clear")
            onClicked: kcm.clearModels()
        }

        Repeater {
            model: kcm.models
            delegate: Kirigami.BasicListItem {
                label: modelData
            }
        }
    }
}

