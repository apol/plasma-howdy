/***************************************************************************
 *   Copyright © 2016 Aleix Pol Gonzalez <aleixpol@blue-systems.com>       *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or         *
 *   modify it under the terms of the GNU General Public License as        *
 *   published by the Free Software Foundation; either version 2 of        *
 *   the License or (at your option) version 3 or any later version        *
 *   accepted by the membership of KDE e.V. (or its successor approved     *
 *   by the membership of KDE e.V.), which shall act as a proxy            *
 *   defined in Section 14 of version 3 of the license.                    *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. *
 ***************************************************************************/

#include <QDebug>
#include <QFile>
#include <QProcess>
#include <QRegularExpression>
#include <KDesktopFile>
#include <KConfigGroup>
#include <kauthhelpersupport.h>
#include <kauthactionreply.h>

using namespace KAuth;

class HowdyHelper : public QObject
{
    Q_OBJECT
public:
    HowdyHelper() {}

    const QString path = QStringLiteral("/usr/lib/security/howdy/config.ini");
    const QString howdy = QStringLiteral("howdy");

public Q_SLOTS:
    KAuth::ActionReply addmodel(const QVariantMap &args)
    {
        const QString name = args[QStringLiteral("name")].toString();

        QProcess p;
        p.start(howdy, {QStringLiteral("add")});
        p.write(name.toUtf8());
        p.waitForFinished(2000);

        ActionReply reply;
        if (p.exitCode() == 0) {
            reply = ActionReply::SuccessReply();
        } else {
            reply = ActionReply::HelperErrorReply(p.exitCode());
            reply.setErrorDescription(QString::fromUtf8(p.readAllStandardOutput()));
        }
        return reply;
    }

    KAuth::ActionReply readconfig(const QVariantMap &)
    {
        ActionReply reply;
        QByteArray replyData;

        if (KDesktopFile::isDesktopFile(path)) {
            KDesktopFile file(path);
            reply = ActionReply::SuccessReply();
            reply.setData({{QStringLiteral("contents"), file.group("core").readEntry("disabled", false)}});
        } else {
            reply = ActionReply::HelperErrorReply();
        }
        return reply;
    }

    KAuth::ActionReply writeconfig(const QVariantMap &args)
    {
        ActionReply reply;
        QByteArray replyData;

        if (KDesktopFile::isDesktopFile(path)) {
            KDesktopFile file(path);
            auto group = file.group("core");
            group.writeEntry("disabled", args[QStringLiteral("disabled")].toBool());
        } else {
            reply = ActionReply::HelperErrorReply();
        }
        return reply;
    }

    KAuth::ActionReply clearmodels(const QVariantMap &/*args*/)
    {
        QProcess p;
        p.start(howdy, {QStringLiteral("clear")});
        p.waitForFinished(2000);

        ActionReply reply;
        if (p.exitCode() == 0) {
            reply = ActionReply::SuccessReply();
        } else {
            reply = ActionReply::HelperErrorReply(p.exitCode());
            reply.setErrorDescription(QString::fromUtf8(p.readAllStandardOutput()));
        }
        return reply;
    }

    KAuth::ActionReply listmodels(const QVariantMap &/*args*/)
    {
        QProcess p;
        p.start(howdy, {QStringLiteral("list")});
        bool b = p.waitForFinished(2000);
        Q_ASSERT(b);

        QTextStream stream(&p);
        QString line;
        QVariantList entries, lines;
        while (stream.readLineInto(&line)) {
            lines.append(line);
            const auto fields = line.splitRef(QLatin1Char('\t'));
            if(fields.count() < 4) {
                continue;
            }

            QVariantMap map;
            map[QStringLiteral("ID")] = fields[0].toString();
            map[QStringLiteral("Date")] = QString(fields[1] + QLatin1Char(' ') + fields[2]);
            map[QStringLiteral("Label")] = fields[3].toString();
            entries.append(map);
            qDebug() << "xxxxxxxx" << line << map;
        }

        ActionReply reply;
//         if (p.exitCode() == 0) {
            reply = ActionReply::SuccessReply();
//         } else {
//             reply = ActionReply::HelperErrorReply(p.exitCode());
//             reply.setErrorDescription(QString::fromUtf8(p.readAllStandardError()));
//         }
        reply.setData({
            {QStringLiteral("user"), qgetenv("USER")},
            {QStringLiteral("models"), entries},
            {QStringLiteral("lines"), lines},
        });
        return reply;
    }
};

KAUTH_HELPER_MAIN("org.kde.plasma.howdy", HowdyHelper)

#include "HowdyHelper.moc"
