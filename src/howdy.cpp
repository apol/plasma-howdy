/*
 * Copyright (C) 2020 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#include "howdy.h"

#include <KSharedConfig>
#include <KConfigGroup>
#include <KPluginFactory>
#include <KAboutData>
#include <KDesktopFile>
#include <KLocalizedString>
#include <KAuth/KAuthAction>
#include <KAuth/KAuthExecuteJob>
#include <QVector>

K_PLUGIN_CLASS_WITH_JSON(Howdy, "kcm_howdy.json");

Howdy::Howdy(QObject *parent, const QVariantList &/*args*/)
    : KQuickAddons::ConfigModule(parent)
{

    setAboutData(new KAboutData(QStringLiteral("kcm_howdy"),
                                       i18n("Facial authentication"),
                                       QStringLiteral("1.0"), i18n("Use your camera to authenticate"), KAboutLicense::LGPL));
    setUseRootOnlyMessage(true);
}

Howdy::~Howdy() = default;

void Howdy::load()
{
    KAuth::Action configAction(QStringLiteral("org.kde.plasma.howdy.readconfig"));
    configAction.setHelperId(QStringLiteral("org.kde.plasma.howdy"));
    Q_ASSERT(configAction.isValid());

    KAuth::ExecuteJob *readConfigJob = configAction.execute();
    connect(readConfigJob, &KAuth::ExecuteJob::result, this, &Howdy::receivedConfig);
    readConfigJob->start();

    setNeedsSave(false);
}

void Howdy::save()
{
    KAuth::Action configAction(QStringLiteral("org.kde.plasma.howdy.writeconfig"));
    configAction.setArguments(m_config);
    configAction.setHelperId(QStringLiteral("org.kde.plasma.howdy"));
    Q_ASSERT(configAction.isValid());

    KAuth::ExecuteJob *writeConfigJob = configAction.execute();
    connect(writeConfigJob, &KAuth::ExecuteJob::result, this, [this] (KJob* job) {
        setNeedsSave(job->error() == 0);
    });
    writeConfigJob->start();
}

void Howdy::defaults()
{
}

void Howdy::receivedConfig(KJob* job)
{
    auto readConfigJob = static_cast<KAuth::ExecuteJob *>(job);
    m_configFailed = job->error() != 0;
    if (m_configFailed) {
        qWarning() << "error loading config" << readConfigJob->errorString() << readConfigJob->action().isValid() << job->error();
    }
    m_config = readConfigJob->data();
}

bool Howdy::isHowdyEnabled() const
{
    return !m_config[QStringLiteral("disabled")].toBool();
}

bool Howdy::isHowdyAvailable() const
{
    return !m_config.isEmpty();
}

void Howdy::setHowdyEnabled(bool enabled)
{
    m_config[QStringLiteral("disabled")] = !enabled;
}

void Howdy::addModel(const QString& name)
{

    KAuth::Action configAction(QStringLiteral("org.kde.plasma.howdy.addmodel"));
    configAction.setArguments({{QStringLiteral("name"), name}});
    configAction.setHelperId(QStringLiteral("org.kde.plasma.howdy"));
    Q_ASSERT(configAction.isValid());

    KAuth::ExecuteJob *addModelJob = configAction.execute();
    connect(addModelJob, &KAuth::ExecuteJob::result, this, [this] (KJob* job) {
        if (job->error() != 0) {
            qWarning() << "failed to add model" << job->error() << job->errorText();
        }
        setNeedsSave(job->error() == 0);
    });
    addModelJob->start();
}

void Howdy::clearModels()
{
    KAuth::Action configAction(QStringLiteral("org.kde.plasma.howdy.clearmodels"));
    configAction.setHelperId(QStringLiteral("org.kde.plasma.howdy"));
    Q_ASSERT(configAction.isValid());

    KAuth::ExecuteJob *clearModelsJob = configAction.execute();
    connect(clearModelsJob, &KAuth::ExecuteJob::result, this, [this] (KJob* job) {
        if (job->error() != 0) {
            qWarning() << "failed to clear models" << job->error() << job->errorText();
        }
        setNeedsSave(job->error() == 0);
    });
    clearModelsJob->start();
}

void Howdy::fetchModels()
{
    KAuth::Action configAction(QStringLiteral("org.kde.plasma.howdy.listmodels"));
    configAction.setHelperId(QStringLiteral("org.kde.plasma.howdy"));
    Q_ASSERT(configAction.isValid());

    KAuth::ExecuteJob *clearModelsJob = configAction.execute();
    connect(clearModelsJob, &KAuth::ExecuteJob::result, this, [this] (KJob* _job) {
        auto job = static_cast<KAuth::ExecuteJob*>(_job);
        if (job->error() != 0) {
            qWarning() << "failed to fetch models" << KAuth::ActionReply::Error(job->error()) << job->errorText() << job->data();
            return;
        }
        qWarning() << "aaaaaaa" << job->data();
        const auto models = job->data().value(QStringLiteral("models")).toList();
        if (m_models != models) {
            m_models = models;
            Q_EMIT modelsChanged(models);
        }
    });
    clearModelsJob->start();
}

QVariantList Howdy::models()
{
    if (m_models.isEmpty())
        fetchModels();
    return m_models;
}

#include "howdy.moc"
