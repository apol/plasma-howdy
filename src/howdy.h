/*
 * Copyright (C) 2020 Aleix Pol Gonzalez <aleixpol@kde.org>
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 *
 *  You should have received a copy of the GNU Library General Public License
 *  along with this library; see the file COPYING.LIB.  If not, write to
 *  the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 *  Boston, MA 02110-1301, USA.
 */

#pragma once

#include <KQuickAddons/ConfigModule>

#include <KDesktopFile>
#include <KJob>

class Howdy : public KQuickAddons::ConfigModule
{
    Q_OBJECT

    Q_PROPERTY(bool isHowdyAvailable READ isHowdyAvailable CONSTANT)
    Q_PROPERTY(bool isHowdyEnabled READ isHowdyEnabled WRITE setHowdyEnabled NOTIFY isHowdyEnabledChanged)
    Q_PROPERTY(QVariantList models READ models NOTIFY modelsChanged)

    public:
        explicit Howdy(QObject* parent = nullptr, const QVariantList &list = QVariantList());
        ~Howdy() override;

        bool isHowdyAvailable() const;
        bool isHowdyEnabled() const;
        void setHowdyEnabled(bool enabled);

        QVariantList models();
        Q_SCRIPTABLE void addModel(const QString &name);
        Q_SCRIPTABLE void clearModels();

    public Q_SLOTS:
        void load() override;
        void save() override;
        void defaults() override;

    Q_SIGNALS:
        void modelsChanged(const QVariantList &models);
        void isHowdyEnabledChanged(bool isHowdyEnabled);

    private:
        void fetchModels();
        void receivedConfig(KJob* job);

        bool m_configFailed = false;
        QVariantList m_models;
        QVariantMap m_config;
};
